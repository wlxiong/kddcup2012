// import and save the characterisctic of users and items
// all the text files are at subdirectory ./track1
// and the data are saved back in format .dta at ./track1
//	lines		files
//	6095		item.txt
//	2320895		user_profile.txt
//	2320895		user_key_word.txt
//	10632833	user_action.txt
//	50655143	user_sns.txt
//	73209277	rec_log_train.txt
//	34910937	rec_log_test.txt

version 11.2
do define_global.do
clear all
set memory $M

capture confirm file track1/user_profile_$K.dta
if _rc | $replace_all == 1 {
	quietly infile long user_id str4 birth_year byte gender long num_tweet str244 user_tags ///
		using track1/user_profile.txt in 1/$K, clear
	desc
	// convert year into numeric
	destring birth_year, replace force
	// convert birth year to age
	gen int age = 2012 - birth_year
	drop birth_year
	// create a sex indicator
	gen byte sex = gender==1
	replace sex = . if gender == 0 | gender == 3 | missing(gender)
	drop gender
	// split user tags
	quietly split user_tags, p(;) gen(user_tag_) destring
	drop user_tags
	// users seldom have more than 10 tags
	drop user_tag_11-user_tag_`r(nvars)'
	// save the data
	recast long user_tag_*
	save track1/user_profile_$K.dta, replace
}

capture confirm file track1/item.dta
if _rc | $replace_all == 1 {
	quietly insheet item_id item_category item_keywords using track1/item.txt, clear
	desc
	// split hierarchical category
	quietly split item_category, p(.) gen(item_cat_) destring
	drop item_category
	// split item keywords
	quietly split item_keywords, p(;) gen(item_key_) destring
	drop item_keywords
	// only use the first 40 keywords
	drop item_key_41-item_key_`r(nvars)'
	// save the data
	recast long item_id item_key_*
	recast byte item_cat_*
	save track1/item.dta, replace
}

capture confirm file track1/user_key_word_$K.dta
if _rc | $replace_all == 1 {
	quietly infile long user_id str244 user_keywords ///
		using track1/user_key_word.txt in 1/$K, clear
	desc
	// split user keyword:wight pairs
	quietly split user_keywords, p(: ;) gen(user_key_weight_) destring
	drop user_keywords
	// users seldom have more than 20 keyword:weight
	drop user_key_weight_41-user_key_weight_`r(nvars)'
	// rename user keywords and weights
	forvalues i = 1/20 {
		local k = `i'*2 - 1
		local w = `i'*2
		rename user_key_weight_`k' user_key_`i'
		rename user_key_weight_`w' user_wei_`i'
	}
	// save the data
	recast long user_key_*
	save track1/user_key_word_$K.dta, replace
}

capture confirm file track1/rec_log_train_$N.dta
	if _rc | $replace_all == 1 {
	quietly infile long user_id long item_id byte result long time ///
		using track1/rec_log_train.txt in 1/$N, clear
	desc
	// create an choice indicator
	gen byte obs_choice = result==1
	replace obs_choice = . if missing(result)
	drop result
	// save the data
	save track1/rec_log_train_$N.dta, replace
}

capture confirm file track1/rec_log_test_$N.dta
if _rc | $replace_all == 1 {
	quietly infile long user_id long item_id byte result long time ///
		using track1/rec_log_test.txt in 1/$N, clear
	desc
	// all the results in test data are zeros, so drop them
	drop result
	// save the data
	save track1/rec_log_test_$N.dta, replace
}
