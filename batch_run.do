// run all the datasets

// define global settings
global replace_all = 0
global max_iter = 50
global LL_gap = 10
global nc = 5

** 2 **
global nb = 2
global dataset_size = 2
global replace_all = 0
do import_dataset.do
global replace_all = 1
do merge_dataset.do
forvalues i = 1/$nb {
	global ss = `i'
	do binary_logit.do
	do predict_choice.do
}

** full dataset **
global nb = 100
global dataset_size = 0
global replace_all = 0
do import_dataset.do
global replace_all = 1
do merge_dataset.do
forvalues i = 1/$nb{
	global ss = `i'
	do binary_logit.do
	do predict_choice.do
}
