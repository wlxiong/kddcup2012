// match item keywords with user keywords

version 11.2
do define_global.do
clear all
set memory $M

** load raw train data
use track1/rec_log_train_profile_$N.dta

// match keywords
capture drop match_key_* total_match_key
rowmatch item_key_*, matchvars(user_key_*) matchindx(match_key)
egen total_match_key = rowtotal(match_key_*)
tab total_match_key

// save dataset
save track1/rec_log_train_profile_$N.dta, replace

** load raw test data
use track1/rec_log_test_profile_$N.dta, clear

// match keywords
capture drop match_key_* total_match_key
rowmatch item_key_*, matchvars(user_key_*) matchindx(match_key)
egen total_match_key = rowtotal(match_key_*)
tab total_match_key

// save dataset
save track1/rec_log_test_profile_$N.dta, replace
