// merge users' choices with their profiles and items' profiles

version 11.2
do define_global.do
clear all
set memory $M

// merge 1:1 user_profile & user_key_word
capture confirm file track1/user_full_profile_$K.dta
if _rc | $replace_all == 1 {
	// load users' profiles
	use track1/user_profile_$K.dta, clear

	// merge with users' keywords
	merge 1:1 user_id using track1/user_key_word_$K.dta
	keep if _merge == 3
	drop _merge

	// save users' full profiles (original profiles + keywords)
	save track1/user_full_profile_$K.dta, replace
}

// merge m:1 rec_log_[train/test] & user_full_profile
capture confirm file track1/rec_log_profile_$N.dta
if _rc | $replace_all == 1 {
	// load training dataset
	use track1/rec_log_train_$N.dta, clear
	// append test dataset
	append using track1/rec_log_test_$N.dta

	// merge with users' full profiles
	merge m:1 user_id using track1/user_full_profile_$K.dta, keepusing(age sex num_tweet)
	keep if _merge == 3
	drop _merge

	// merge with items' profiles
	merge m:1 item_id using track1/item.dta, keepusing(item_cat_*)
	keep if _merge == 3
	drop _merge

	// // match user and item keywords
	// rowmatch item_key_*, matchvars(user_key_*) matchindx(match_key)
	// egen int total_match_key = rowtotal(match_key_*)
	// tab total_match_key
	// 
	// // calculate keyword weight sum
	// unab key_vars : match_key_*
	// local nkey: word count `key_vars'
	// gen double key_wei_sum = 0
	// forvalues i = 1/`nkey' {
	// 	qui mvencode user_wei_`i', mv(0) override
	// 	replace key_wei_sum = key_wei_sum + match_key_`i'*user_wei_`i'
	// }
	
	// back up the original order
	gen long original_order = _n
	
	// select one observation for all user
	egen tag_all_user = tag(user_id)
	// split the sample into $nb subsamps
	gen rand = runiform()
	sort rand
	qui gen byte subsamp = group($nb) if tag_all_user
	qui bysort user_id (tag_all_user): replace subsamp = subsamp[_N]
	drop rand
	
	// save the training/test profiles
	save track1/rec_log_profile_$N.dta, replace
}
