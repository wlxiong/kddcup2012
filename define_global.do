// define all the global macros here

capture macro list nc 
if _rc {
	// number of laent classes
	global nc = 2
}

capture macro list max_iter
if _rc {
	global max_iter = 5
}

capture macro list LL_gap
if _rc {
	global LL_gap = 1.0
}

capture macro list replace_all
if _rc {
	// default action: replace all the existing files
	global replace_all 1
}

capture macro list dataset_size
if _rc {
	// default action: import the smallest dataset
	global dataset_size 1
}

capture macro drop K N M

if $dataset_size == 0 {
	// import upto the last users
	global K "l"
	// import upto the last choices
	global N "l"
	global M 10g
}
else if $dataset_size == 1 {
	// import the first K users 
	global K 100000
	// and import the first N choices
	global N 1000000
	global M 1g
}
else if $dataset_size == 2 {
	global K 200000
	global N 2000000
	global M 2g
}
else if $dataset_size == 5 {
	global K 500000
	global N 5000000
	global M 10g
}
