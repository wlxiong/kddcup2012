// estimate a latent class logit model

version 11.2
do define_global.do
clear all
set memory $M
set matsize 5000
set more off
capture log close
log using "track1/binary_logit_($nc)($ss,$nb)$N.log", replace
set seed 314159265

// load train profiles
use track1/rec_log_profile_$N.dta, clear
// drop user_tag_*
// drop user_key_*
// drop user_wei_*
// drop item_key_*
// drop match_key_*

keep if subsamp == $ss

// define choice event
bysort user_id time: gen long choice_alt = _n
gen long event = user_id*100 + choice_alt

// select one observation for each choice occasion
egen tag_event = tag(event) if !missing(obs_choice)
// select one observation for users in training dataset
egen tag_user = tag(user_id) if !missing(obs_choice)

// mark observations with complete entries
egen num_rmiss = rowmiss(obs_choice age sex)
gen byte tag_obs = num_rmiss == 0
tab tag_obs num_rmiss
tab obs_choice, missing
drop num_rmiss

// substitute the missing age with sample average
su age if tag_all_user
global mean_age = r(mean)
replace age = int($mean_age) if missing(age)
// calculate the gender rate of the sample
su sex if tag_all_user
global sex_rate = r(mean)
// generate random sex for missing values
replace sex = cond(runiform()<=$sex_rate, 1, 0) if missing(sex) & tag_all_user
bysort user_id (tag_all_user): replace sex = sex[_N] if missing(sex)
// substitute the missing item category with a dummy one
forvalues i = 1/4 {
	replace item_cat_`i' = 99 if missing(item_cat_`i')
}

// number of choice alternatives and positive responses
// bysort event: gen long numalts = _N if tag_event
// bysort event: egen long numones = total(obs_choice)
// replace numones = . if !tag_event
// tab numalts numones, missing
// drop numalts numones

// create a dummy alternative for each choice occasion with all negative response
// expand 2 if tag_event & numones == 0, generate(dummy_alt)
// tab dummy_alt, missing
// replace tag_event = 0 if dummy_alt
// replace tag_user = 0 if dummy_alt
// // generate the attributes of the dummy alternative
// replace item_id = 0 if dummy_alt
// replace obs_choice = 1 if dummy_alt
// // replace key_wei_sum = 0 if dummy_alt
// forvalues i = 1/4 {
// 	replace item_cat_`i' = 0 if dummy_alt
// }

// number of choice alternatives and positive responses with dummy alt
// bysort event: gen long numalts_dummy = _N if tag_event
// bysort event: egen long numones_dummy = total(obs_choice)
// replace numones_dummy = . if !tag_event
// tab numalts_dummy numones_dummy, missing
// drop numalts_dummy numones_dummy

// category indicators
global cat_indx " "
forvalues i = 1/4 {
	global cat_indx "$cat_indx i.item_cat_`i'"
}

// demographic variables
global user_demo "age sex num_tweet"

// define the dependent and independent variables
global y obs_choice
global X $cat_indx
// global X key_wei_sum $cat_indx
display "$y"
display "$X"


** EM algorithm for latent class logit model **

** split the sample into $nc subsets
gen rand = runiform()
sort rand
qui gen byte subset = group($nc) if tag_user
qui bysort user_id (tag_user): replace subset = subset[_N]
drop rand

** get $nc starting values for the parameters
forvalues s = 1/$nc {
	gen double class_prob_`s' = 1.0/$nc
	clogit $y $X [iw=class_prob_`s'] if tag_obs & subset == `s', group(event) tech(nr dfp)
	predict double xb_`s', xb
	qui gen double pr_`s' = invlogit(xb_`s') if $y == 1
	qui replace pr_`s' = invlogit(-xb_`s') if $y == 0
}

// initialize log likelihood value
global LL0 = 0
global LL1 = 100
local k = 0

** iteration starts **
while `k' < $max_iter & abs($LL1 - $LL0) > $LL_gap {

disp "**********************"
disp "** `k'-th iteration **"
disp "**********************"
disp "latest LL: $LL1, $LL0"

** the probability of the choice conditional on user class
capture drop choice_prob_*
forvalues s = 1/$nc {
	egen double choice_prob_`s' = prod(pr_`s'), by(user_id)
	replace choice_prob_`s' = . if !tag_user
}

** the unconditional choice probability
capture drop uncond_choice_prob
gen double uncond_choice_prob = 0 if tag_user
forvalues s = 1/$nc {
	qui bysort user_id: replace uncond_choice_prob = ///
		uncond_choice_prob + class_prob_`s'*choice_prob_`s' if tag_user
}

** the probability of class membership conditional on choices
capture drop cond_class_prob_*
forvalues s = 1/$nc {
	qui bysort user_id: gen double cond_class_prob_`s' = ///
		class_prob_`s'*choice_prob_`s' / uncond_choice_prob if tag_user
}

** prepare a multinomial logit model with grouped data
capture drop latent_class user_class weight
expand $nc if tag_user, gen(latent_class)
bysort tag_user user_id: gen byte user_class = _n if tag_user
gen double weight = 0 if tag_user
forvalues s = 1/$nc {
	replace weight = cond_class_prob_`s' if tag_user & user_class == `s'
}

** estimate the parameters of class membership
mlogit user_class $user_demo [iw=weight] if tag_obs & tag_user, b(1)
** predict and spread the probability of class membership
capture drop class_prob_*
forvalues s = 1/$nc {
	predict double class_prob_`s', outcome(`s')
}
// drop the expanded observations for each latent class
drop if latent_class

** calculate the choice probability
capture drop xb_* pr_*
forvalues s = 1/$nc {
	clogit $y $X [iw=class_prob_`s'] if tag_obs, group(event) tech(nr dfp)
	predict double xb_`s', xb
	qui gen double pr_`s' = invlogit(xb_`s') if $y == 1
	qui replace pr_`s' = invlogit(-xb_`s') if $y == 0
}

** calculate log likelihood
global LL0 = $LL1
gen double LL = ln(uncond_choice_prob)
qui su LL
drop LL
global LL_`k' = r(sum)
global LL1 = ${LL_`k'}
local k = `k' + 1

}

** iteration ends **
if abs($LL1 - $LL0) <= $LL_gap {
	disp "Convergence reached! "
	disp "Log likelihood = $LL1"
}
else {
	disp "Maximum iteration ($max_iter) reached! "
	disp "Latest LL: $LL1, $LL0"
}

// drop the dummy alternative
// drop if dummy_alt

// save estimation results
save "track1/rec_log_result_($nc)($ss,$nb)$N.dta", replace
log close
