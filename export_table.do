// combine all the subsamples and export to a csv file

version 11.2
do define_global.do
clear all
set memory $M

forvalues i = 1/$nb{
	append "track1/rec_log_result_($nc)(`i',$nb)$N.dta"
}

// restore the original order
sort original_order

// print to a csv file
outsheet user_id top3_items using "track1/rec_log_ranking_($nc)($nb)$N.csv" if tag_all_user, comma replace

// save rankings
save "track1/rec_log_result_($nc)($nb)$N.dta"
