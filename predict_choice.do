capture log close
log using "track1/predict_choice_($nc)($ss,$nb)$N.log", replace

// load estimation results
// use "track1/rec_log_result_($nc)($ss,$nb)$N.dta", clear

** predict users' choices

// the postive response probability conditional on class membership
capture drop pc1_*
forvalues s = 1/$nc {
	predict double pc1_`s', pc1
}

// the unconditional choice probability
capture drop uncond_pc1
gen double uncond_pc1 = 0
forvalues s = 1/$nc {
	replace uncond_pc1 = uncond_pc1 + class_prob_`s'*pc1_`s'
}
su uncond_pc1

// predicted choices
local rank_pref "uncond_pc1" // conditional on one positive response
local prob_thres = 0.30
tab obs_choice, missing
su user_id item_id `rank_pref' if `rank_pref' > `prob_thres' & obs_choice == 1
su user_id item_id `rank_pref' if `rank_pref' > `prob_thres' & obs_choice == 0
su user_id item_id `rank_pref' if `rank_pref' < `prob_thres' & obs_choice == 1
su user_id item_id `rank_pref' if `rank_pref' > `prob_thres' & missing(obs_choice)

** export the results

// get the top three items
capture drop ordered_item_*
bysort user_id (`rank_pref'): gen ordered_item_1 = item_id[_N  ] if tag_all_user
bysort user_id (`rank_pref'): gen ordered_item_2 = item_id[_N-1] if tag_all_user & _N > 1
bysort user_id (`rank_pref'): gen ordered_item_3 = item_id[_N-2] if tag_all_user & _N > 2

// convert to a string
tostring ordered_item_*, replace
replace ordered_item_1 = "" if ordered_item_1 == "." & tag_all_user
replace ordered_item_2 = "" if ordered_item_2 == "." & tag_all_user
replace ordered_item_3 = "" if ordered_item_3 == "." & tag_all_user
capture drop top3_items
egen top3_items = concat(ordered_item_*) if tag_all_user, punct(" ")
drop ordered_item_*

// restore the original order
// sort original_order

// print to a csv file
outsheet user_id top3_items using "track1/rec_log_ranking_($nc)($ss,$nb)$N.csv" if tag_all_user, comma replace

// save item rankings
save "track1/rec_log_ranking_($nc)($ss,$nb)$N.dta", replace
log close
