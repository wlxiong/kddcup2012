*! rowmatch 1.0		WLX 27 March 2012
program rowmatch
	version 11.2
	syntax varlist(min=1 numeric) [if] [in], ///
		matchvars(varlist min=1 numeric) matchindx(str)
	display as txt "`varlist'"
	display as txt "`matchvars'"
	display as txt "`matchindx'"

	quietly {
		marksample touse
		count if `touse'
		if r(N) == 0 error 2000

		foreach v of local varlist {
			capture assert `v' == int(`v') if `touse'
			if _rc {
				disp as err "`v' bad: integer variables required"
				exit 498
			}
		}

		foreach v of local matchvars {
			capture assert `v' == int(`v') if `touse'
			if _rc {
				disp as err "`v' bad: integer variables required"
				exit 498
			}
		}
	}

	local matchindxvars " "
	local nvars: word count `matchvars'
	forvalues i = 1/`nvars'{
		// validate new variable names
		confirm new variable `matchindx'_`i'
		// create match index variable
		qui gen byte `matchindx'_`i' = 0
		local matchindxvars "`matchindxvars' `matchindx'_`i'"
	}

	mata: mf_rowmatch("`varlist'", "`matchvars'", "`matchindxvars'", "`touse'")

end

mata: mata clear
mata: mata set matastrict on
version 11.2
mata:
// mf_rowmatch 1.0		1.0, WLX 27 March 2012
void function mf_rowmatch(string scalar mastervars,
						  string scalar matchvars,
						  string scalar matchindx,
						  string scalar touse)
{
	real matrix V, W, I
	string matrix vv, ww, xx
	real scalar nobs, r, i, j

	vv = tokens(mastervars)
	ww = tokens(matchvars)
	xx = tokens(matchindx)
	nobs = st_nobs()

	for (r = 1; r <= nobs; r++){
		// get data from the current dataset
		V = st_data(r, vv, touse)
		W = st_data(r, ww, touse)
		X = st_data(r, xx, touse)
		// skip empty rows
		if (length(V) == 0 | length(W) == 0) continue
		// sort the row vectors
		V = sort(V',1)'
		W = sort(W',1)'
		// remove the missing entries
		V = V[1..nonmissing(V)]
		W = W[1..nonmissing(W)]
		if (length(V) == 0 | length(W) == 0) continue
		// match mastervec with matchvec
		i = 1
		j = 1
		while (i <= length(V) & j <= length(W)){
			if (V[i] < W[j]) i++
			else if (V[i] > W[j]) j++
			else{
				X[j] = 1
				i++
				j++
			}
		}
		// store match index in the dataset
		st_store(r, xx, touse, X)
	}
}
end
